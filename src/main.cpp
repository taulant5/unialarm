#include <iostream>
#include <thread>
#include "../header/iCalParser.hpp"


std::string durationToString(std::chrono::duration<float> dur)
{
    auto days = std::chrono::duration_cast<std::chrono::duration<int, std::ratio<86400>>>(dur);
    dur -= days;
    auto hours = std::chrono::duration_cast<std::chrono::hours>(dur);
    dur -= hours;
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(dur);


    std::string returnVal;


    if(days.count() != 0)
        returnVal += std::to_string(days.count()) + "Days ";
    if(hours.count() != 0)
        returnVal += std::to_string(hours.count()) +"Hours ";
    if(minutes.count() != 0)
        returnVal += std::to_string(minutes.count()) +"Min";
    return returnVal;
}




int main()
{

    ICalHandler test = load("timetable.ics");
    test.sortBy(test.BEGIN, true);
    std::vector<ICalEvent> events;
    for(auto &t : test.getEvents())
        if(t.getBegin() >= std::chrono::system_clock::now())
            events.push_back(t);
    
    if(events.size() > 0)
    {
        ICalEvent e = events[0];
        auto difference = e.getBegin() - std::chrono::system_clock::now();
        if(difference >= std::chrono::hours(8)) //Next cronjob will do the job or I will be awake
            return 1;
        std::this_thread::sleep_for(difference - std::chrono::hours(2));
        std::cout << "Next lesson in 2 hours." << std::endl;
        if(e.has("SUMMARY"))
            std::cout << e.get("SUMMARY") << std::endl;
       return 0;
    }


    return 1;
}

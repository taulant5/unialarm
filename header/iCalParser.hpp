#include "iCalHandler.hpp"
#include <fstream>


int timediff = 2;



void setKeyValue(ICalEvent& e, const std::string& keyValue)
{
    auto search = keyValue.find(":");
    if(search != std::string::npos)
        e.set(keyValue.substr(0, search), keyValue.substr(search + 1));
}



std::chrono::time_point<std::chrono::system_clock> convertToTime(std::string line)
{
    auto search = line.find(":");
    line = line.substr(search + 1, line.length() - search - 1);
    int year = std::stoi(line.substr(0, 4));
    int mon = std::stoi(line.substr(4, 2));
    int day = std::stoi(line.substr(6, 2));
    int hour = std::stoi(line.substr(9, 2));
    int min = std::stoi(line.substr(11, 2));
    int sec = std::stoi(line.substr(13, 2));

    struct std::tm returnVal;
    returnVal.tm_year = year - 1900;
    returnVal.tm_mon = mon - 1;
    returnVal.tm_mday = day;
    returnVal.tm_hour = hour + timediff;
    returnVal.tm_min = min;
    returnVal.tm_sec = sec;
    returnVal.tm_isdst = 1;
    return std::chrono::system_clock::from_time_t(std::mktime(&returnVal));
}

void cleanString(std::string& input)
{
    input.erase(std::remove(input.begin(), input.end(), '\n'), input.end());
    input.erase(std::remove(input.begin(), input.end(), '\t'), input.end());
    input.erase(std::remove(input.begin(), input.end(), '\r'), input.end());
}




ICalHandler load(const std::string& filepath)
{
    std::ifstream file(filepath); 
    if (!file.is_open()) 
        throw std::string("Can't find ") + filepath;
    ICalHandler handler;
    std::string line;
    while(std::getline(file, line))
    {
        if(line.find("BEGIN:VEVENT") == 0)
        {
            ICalEvent event;
            std::string prevLine;
            //use prevSet to be able to read multiple lines
            bool prevSet = false;
            while(std::getline(file, line))
            {
                if(line.find("END:VEVENT") == 0)
                {   if(prevSet)
                        setKeyValue(event, prevLine);
                    break;
                }
                if(line.find("DTSTART") == 0)
                {
                    event.setBegin(convertToTime(line));
                    if(prevSet)
                        setKeyValue(event, prevLine);
                    prevSet = false;
                    continue;
                }
                if(line.find("DTEND") == 0)
                {
                    event.setEnd(convertToTime(line));
                    if(prevSet)
                        setKeyValue(event, prevLine);
                    prevSet = false;
                    continue;
                }
                cleanString(line);
                if(line.find(" ") == 0)
                    if(!prevSet)
                        throw std::string("this can't be appended anywhere");
                    else
                        line = prevLine + line.substr(1); //append the read line to previous line
                else
                    if(prevSet)
                        setKeyValue(event, prevLine); //only set the previous line if a new line has started for sure
                prevLine = line;
                prevSet = true;
            }
            handler.addEvent(event);
        }
    }
    return handler;
}

#include "iCalEvent.hpp"
#include <algorithm>
#include <vector>

class ICalHandler {
    private:
        std::vector<ICalEvent> m_Events;
    public:
        void addEvent(const ICalEvent& event)
        {
            m_Events.push_back(event);
        }

        enum property 
        {
            BEGIN,
            DURATION,
            END
        };
        
        void sortBy(property p, bool ascending)
        {
                    std::sort(m_Events.begin(), m_Events.end(), [ascending, p](ICalEvent& a, ICalEvent& b)
                            {
                                switch(p)
                                {
                                    case BEGIN:
                                        return ascending?a.getBegin() < b.getBegin():a.getBegin() > b.getBegin();

                                    case END:
                                        return ascending?a.getEnd() < b.getEnd():a.getEnd() < b.getEnd();

                                    case DURATION:
                                        std::chrono::duration<float> first,second;
                                        first = a.getEnd() - a.getBegin();
                                        second = b.getEnd() - b.getEnd();
                                        return ascending?first < second:first > second;

                                    default:
                                        throw std::string("Unsupported");
                                }
                            });
        }

        const std::vector<ICalEvent>& getEvents() const
        {
            return m_Events;
        }



        ICalHandler findProperty(std::string& key, std::string& value) const
        {
            ICalHandler temp;
            for(const ICalEvent& event: m_Events)
                if(event.has(key))
                    if(event.get(key) == value)
                        temp.addEvent(event);
            return temp;
        }



        



};

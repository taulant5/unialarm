#include <string>
#include <map>
#include <chrono>

class ICalEvent
{
    private:
        std::chrono::time_point<std::chrono::system_clock> m_begin;
        std::chrono::time_point<std::chrono::system_clock> m_end;
        std::map<std::string, std::string> m_values;
        bool beginSet;
        bool endSet;
    public:
        ICalEvent(const std::chrono::time_point<std::chrono::system_clock>& begin, const std::chrono::time_point<std::chrono::system_clock>& end):m_begin(begin), m_end(end), beginSet(true), endSet(true)
        {
        }


        ICalEvent():beginSet(false), endSet(false)
        {
        }


        void setBegin(const std::chrono::time_point<std::chrono::system_clock>& begin)
        {
            if(!beginSet)
            {
                m_begin = begin;
                beginSet = true;
            }
            else
                throw std::string("begin already set");
        }

        void setEnd(const std::chrono::time_point<std::chrono::system_clock>& end)
        {
            if(!endSet)
            {
                m_end = end;
                endSet = true;
            }
            else
                throw std::string("end already set");
        }


        void set(const std::string& key, const std::string& value)
        {
            if(has(key))
               throw key + std::string(" already in use");
            m_values[key] = value;
        }
        bool has(const std::string& key) const
        {
            auto search = m_values.find(key);
            return search != m_values.end();
        }

        const std::string& get(const std::string& key) const
        {
            auto search = m_values.find(key);
            if(search == m_values.end())
                throw key + std::string(" not found");
            return search->second;
        }



        const std::chrono::time_point<std::chrono::system_clock>& getBegin() const
        {
            return m_begin;
        }

        const std::chrono::time_point<std::chrono::system_clock>& getEnd() const
        {
            return m_end;
        }





};

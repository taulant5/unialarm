#!/bin/bash
curl "URL" --output timetable.ics
if output=$(./UniAlarm); then
    weather="$(curl -s wttr.in/CITY?0FQT | sed -E 's/^.{15}//')"
    temperature=$(echo "$weather" | sed -n '2p' | tr -cd '[[:digit:]]')
    description=$(echo "$weather" | sed -n '1p')

    echo "Good morning XY, it is ${description} today. Current temperatures are ${temperature} degrees .. ${output}" | festival --tts
fi
